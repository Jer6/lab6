/**********************
  James Robertson
  jer6
  Lab 2
  Lab Section: 004
  Anurata Hridi
************************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
  int i, j;
  int count = 0;
  Card deck[52];

  for (i = 0; i < 4; i++) {
    for (j = 2; j <= 14; j++) {
      deck[count].suit = static_cast<Suit>(i);
      deck[count].value = j;
      count++;
    }
  }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
  random_shuffle(deck, deck + 52, myrandom);

  /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
  Card hand[5] = { deck[0], deck[1], deck[2], deck[3], deck[4] };

  /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
  sort(hand, hand + 5, suit_order);

  /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
  for (i = 0; i < 5; i++) {
    cout << setw(10) << get_card_name(hand[i]);
    cout << " of ";
    cout << get_suit_code(hand[i]) << endl;
  }



  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {

  if (lhs.suit < rhs.suit) {
    return true;

  }else if (lhs.suit == rhs.suit) {

    if (lhs.value < rhs.value) {
      return true;

    }else{
      return false;

    }
  }
  return false;
}

//Returns string code needed to print proper suit symbol of card
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//Returns a string for the name of the card based on its value
string get_card_name(Card& c) {
  switch (c.value) {
    case 2:         return "2";
    case 3:         return "3";
    case 4:         return "4";
    case 5:         return "5";
    case 6:         return "6";
    case 7:         return "7";
    case 8:         return "8";
    case 9:         return "9";
    case 10:        return "10";
    case 11:        return "Jack";
    case 12:        return "Queen";
    case 13:        return "King";
    case 14:        return "Ace";
    default:        return "";
  }
}
